import math
import re


CARDS_PER_ROW = 4
CARDS_PER_PAGE = 8


def escape(text):

    # Escape special characters
    for char in ["#"]:  # ["&", "%", "$", "#", "{", "}"]:
        text = re.sub(char, rf"\\{char}", text)

    # Swap underscores for special "\blank" command
    text = re.sub("_", r"\\blank", text)

    return text

def main():
    with open("black_cards.txt", "r") as file:
        black_cards = file.read().split("\n")

    with open("white_cards.txt", "r") as file:
        white_cards = file.read().split("\n")

    with open("preamble.tex", "r") as file:
        tex = file.read()

    tex += "\\begin{document}\n"

    for card_type in ["black", "white"]:

        cards = black_cards if card_type == "black" else white_cards
        num_pages = math.ceil(len(cards) / 8)

        for i in range(num_pages):

            cards_on_page = cards[i*CARDS_PER_PAGE:(i+1)*CARDS_PER_PAGE]
            num_cards_on_page = len(cards_on_page)

            # Front of cards
            for j in range(CARDS_PER_PAGE):
                if j < num_cards_on_page:
                    card_text = cards[(i*CARDS_PER_PAGE)+j]
                    tex += f"\\{card_type}card{{{escape(card_text)}}}%\n"
                else:
                    tex += "\\cardplaceholder%\n"
                if j in [CARDS_PER_ROW-1]:
                    tex += "\n\\cardrowvspacing%\n\n"

            tex += "\n\\pagebreak%\n\n"

            # Reverse of cards
            for j in range(CARDS_PER_PAGE):
                j_flipped = CARDS_PER_ROW * (1 + j // CARDS_PER_ROW) - (1 + j % CARDS_PER_ROW)
                if j_flipped < num_cards_on_page:
                    tex += f"\\{card_type}cardback%\n"
                else:
                    tex += "\\whitecardback%\n"
                if j in [CARDS_PER_ROW-1]:
                    tex += "\n\\cardrowvspacing%\n\n"

            tex += "\n\\pagebreak%\n\n"

    tex += "\\end{document}\n"

    with open("output/cards.tex", "w") as file:
        file.write(tex)


if __name__ == '__main__':
    main()
